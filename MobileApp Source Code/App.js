import React from 'react';
import { StackNavigator } from 'react-navigation';
import HomeScreen from './screens/HomeScreen'
import ImageScreen from './screens/ImageScreen'

/*
  Sir, ang pag install nako sa React Navigation kay 'npm install --save react-navigation.1.0.0 beta.11'
  kay dili mugana ang 'npm install --save react-navigation' kay naa siyay gina remove na files
  Tapos, ang pag import nako sa React Navigation kay 'StackNavigator' na dili na
  'createStackNavigator' lahi man gud daw ang version na gamit nako, kay nag specify ko sa version
  sa react-navigation.
*/

const RootStack = StackNavigator (
  {
    Home: {
      screen: HomeScreen,
    },
    Image: {
      screen: ImageScreen,
    },
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}