import React, { Component } from 'react';
import { View, Image, Button, StyleSheet } from 'react-native'

export default class ImageScreen extends Component {
    render() {
        return (
            <View >
                <Image style={{ width: 50, height: 50 }}
                    source={require('./images/PlanToGo_Logo.png')}
                />
                <Button
                    title="Home"
                    onPress={() => this.props.navigation.navigate('Home')}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 20,
      margin: 20,
      textAlign: 'center',
    },
    space: {
      margin: 20,
    }
  });
