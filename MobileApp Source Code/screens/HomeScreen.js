import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, ScrollView } from 'react-native';

class HomeScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
              <Text style={styles.title}>Welcome</Text>
              <Button
                    title="Home"
                    onPress={() => this.props.navigation.navigate('Image')}
                />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 20,
      margin: 20,
      textAlign: 'center',
    },
  });

export default HomeScreen;
